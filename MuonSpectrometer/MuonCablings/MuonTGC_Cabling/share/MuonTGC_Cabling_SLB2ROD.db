*----------------------------------------------------------------------
* SLB2ROD.db
*-----------
* Connection information between SLBs and RODs through SSWs
*
* SSW
*  ALL
*   [SSW id][ROD connector]
*  
* SLB
*  type [total SLBs]
*   [sector id][SLB id][SBLoc][SLB addr][SSW id][SSW block]
*  
*  Note
*   A SSW for Inner cover 1/4 wheel (3 sectors)
*    SSW block for sector n () is given by
*      EI    0 + (n % 6)*2
*      FI    1 + (n % 6)*2
*    SSW Id for for sector n is given by
*      Id = 8 + (n/6);
*
*   Correct SBLocs for EIFI used in hardware are 
*    for EWI and ESI : 8 and 9 
*    for FWI and FSI : 0 and 1 
*    bug #57051: Wrong numbering of SBLOC for Inner Stations 
*                (EI/FI) in 12-fold TGC cablings
*   However, there is a backward compatibility issue. 
*   We will use the following SBLocs for offline. 
*    For EWI and ESI : 1 and 3 
*    For FWI and FSI : 0 and 2 
*   SBLocs (slbIds) of EIFI are intentionally changed in BS<->RDO conversions. 
*    For BS->RDO : byteStream2Rdo of MuonTGC_CnvTools/src/TGC_RodDecoderReadout.cxx 
*    For RDO->BS : rdo2ByteStream of MuonTGC_CnvTools/src/TgcByteStream.cxx
*  
*                  Date    Who  Explanation of Changes
*               --------- ----- ---------------------------------------
* Version  3.00  8-Aug-07 HK    version for New TGCCabling
*                               Add SBLoc
* Version  3.01 25-Aug-07 HK    Fix FWT RxID 
* Version  3.02 22-Aug-07 HK    Fix EST phi1/3 RxId 
* Version  3.03 15-Jan 08 HK    Add SLB on SL
* Version  3.04  6-Aug 08 HK    Fix Inner RxID/sbLoc/SLBaddr
* Version  3.05 12-Oct 09 HK    Fix Inner sbLoc
* Version  3.06 17-Oct 10 SO    Reverted the change of 3.05 and added note. 
*---------------------------------------------------------------------- 
SSW
 ALL
  0 0
  1 1
  2 2
  3 3
  4 4
  5 5
  6 6
  7 7
  8 8
  9 9   
SLB
 EWT 7
  0 0  0  0   0 18
  0 1  1  1   0 17
  0 2  2  2   0 16
  0 3  3  3   0  3
  0 4  4  4   0  2
  0 5  5  5   0  7
  0 6  6  6   0  6
  1 0  8 16   0 19
  1 1  9 17   0 15
  1 2 10 18   0 14
  1 3 11 19   0  1
  1 4 12 20   0  0
  1 5 13 21   0  5
  1 6 14 22   0  4
  2 0  0  0   1 18
  2 1  1  1   1 17
  2 2  2  2   1 16
  2 3  3  3   1  3
  2 4  4  4   1  2
  2 5  5  5   1  7
  2 6  6  6   1  6
  3 0  8 16   1 19
  3 1  9 17   1 15
  3 2 10 18   1 14
  3 3 11 19   1  1
  3 4 12 20   1  0
  3 5 13 21   1  5
  3 6 14 22   1  4 
 EST 2
  0 0 16 25   0 11
  0 1 17 26   0 10
  1 0 24 23   0  9 
  1 1 25 24   0  8 
  2 0 16 25   1 11
  2 1 17 26   1 10
  3 0 24 23   1  9 
  3 1 25 24   1  8 
 FWT 4
  0 0  0  7   2  1 
  0 1  1  8   2  0 
  0 2  2  9   2 18
  0 3  3 10   2 17
  1 0  8  7   2  3 
  1 1  9  8   2  2 
  1 2 10  9   2 22
  1 3 11 10   2 21 
 FST 1
  0 0 16 11   2 16
  1 0 24 11   2 20 
 EWD 10
  0 0  0  0   3  1
  0 1  1  1   3  0
  0 2  2  2   3  3
  0 3  3  3   3  2
  0 4  4  4   3  5
  0 5  5  5   3  4
  0 6  6  6   3  7
  0 7  7  7   3  6
  0 8  8  8   3  9
  0 9  9  9   3  8
  1 0  0  0   4  1
  1 1  1  1   4  0
  1 2  2  2   4  3
  1 3  3  3   4  2
  1 4  4  4   4  5
  1 5  5  5   4  4
  1 6  6  6   4  7
  1 7  7  7   4  6
  1 8  8  8   4  9
  1 9  9  9   4  8
  2 0  0  0   5  1
  2 1  1  1   5  0
  2 2  2  2   5  3
  2 3  3  3   5  2
  2 4  4  4   5  5
  2 5  5  5   5  4
  2 6  6  6   5  7
  2 7  7  7   5  6
  2 8  8  8   5  9
  2 9  9  9   5  8
  3 0  0  0   6  1
  3 1  1  1   6  0
  3 2  2  2   6  3
  3 3  3  3   6  2
  3 4  4  4   6  5
  3 5  5  5   6  4
  3 6  6  6   6  7
  3 7  7  7   6  6
  3 8  8  8   6  9
  3 9  9  9   6  8 
 ESD 5
  0 0 16 10   3 11
  0 1 17 11   3 10
  0 2 18 12   3 18
  0 3 19 13   3 17
  0 4 20 14   3 16
  1 0 16 10   4 11
  1 1 17 11   4 10
  1 2 18 12   4 18
  1 3 19 13   4 17
  1 4 20 14   4 16
  2 0 16 10   5 11
  2 1 17 11   5 10
  2 2 18 12   5 18
  2 3 19 13   5 17
  2 4 20 14   5 16
  3 0 16 10   6 11
  3 1 17 11   6 10
  3 2 18 12   6 18
  3 3 19 13   6 17
  3 4 20 14   6 16 
 FWD 4
  0 0  0 16   7  1
  0 1  1 17   7  0
  0 2  2 18   7  3
  0 3  3 19   7  2
  1 0  8 16   7  5
  1 1  9 17   7  4
  1 2 10 18   7  7
  1 3 11 19   7  6
 FSD 2 
  0 0 16 20   7  8
  1 0 24 20   7 10  
 EWI 2 
  0 0  1  6   8  1
  1 0  3  7   8  3 
 ESI 2 
  0 0  1  6   8  1
  1 0  3  7   8  3
 FWI 2 
  0 0  0  0   8  0
  1 0  2  1   8  2
 FSI 2 
  0 0  0  0   8  0
  1 0  2  1   8  2 
 ESL 4 
  0 0  0  0   9  0
  1 1  1  1   9  1
  2 2  2  2   9  2
  3 3  3  3   9  3   
 FSL 2 
  0 0  4  4   9  4
  1 1  5  5   9  5
